package com.gi.bebas;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener {
    private TextView txregister;
    private Button btlogin;
    ///////////////////////
    private Activity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //menghilangkan toolbar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_login);

        ///--yourcode---2/5/2018
        txregister = (TextView) findViewById(R.id.lltxregister);
        btlogin = (Button) findViewById(R.id.llbtlogin);

        btlogin.setOnClickListener(this);
        txregister.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llbtlogin:{
                Intent intent = new Intent(activity, ActivityBeranda.class);
                startActivity(intent);
                break;
            }case R.id.lltxregister:{
                Intent intent =  new Intent(activity, ActivityRegister.class);
                startActivity(intent);
                break;
            }
        }

    }
}
