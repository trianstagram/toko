package com.gi.bebas;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ActivityDetail extends AppCompatActivity {

    ImageView ivmakanan;
    TextView tvjudul, tvhargasebelum, tvhargasesudah, tvalamat, tvdiskon, tvdeskripsi;

    private String judul, harga, alamat, diskon, deskripsi;
    private int harga_sebelum_diskon, harga_setelah_diskon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_detail);

        //Munculin Tombol back di action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ivmakanan = findViewById(R.id.rlivmakanan);
        tvjudul = findViewById(R.id.rltvnamamakanan);
        tvhargasebelum = findViewById(R.id.rltvhargamakanansebelum);
        tvhargasesudah = findViewById(R.id.rltvhargamakanansesudah);
        tvalamat = findViewById(R.id.rltvalamatmakanan);
        tvdiskon = findViewById(R.id.rltvdiskon);
        tvdeskripsi = findViewById(R.id.lltvdesc);

        //1000000   => 1.000.000
        NumberFormat uang = new DecimalFormat("#,###");

        harga_sebelum_diskon = 10000;
        harga_setelah_diskon = 20000;
        tvhargasebelum.setText(uang.format(harga_sebelum_diskon));
        tvhargasesudah.setText(uang.format(harga_setelah_diskon));
    }


    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
