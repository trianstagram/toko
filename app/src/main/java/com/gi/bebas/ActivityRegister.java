package com.gi.bebas;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

public class ActivityRegister extends AppCompatActivity implements View.OnClickListener {
    private Activity activity = this;
    private Button btRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);

        btRegister = (Button) findViewById(R.id.llbtregister);
        btRegister.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llbtregister:{
                Intent intent =  new Intent(activity, ActivityLogin.class);
                startActivity(intent);
            }
        }
    }
}
